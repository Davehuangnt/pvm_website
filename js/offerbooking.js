/* 
顶部导航 BOOK NOW
*/

    $(function() {
    $('.spinnerRooms1').spinner({value:1,min:1,max:9});
    $('.spinnerAdults1').spinner({value:2,min:1,max:3});
    $('.spinnerChild1').spinner({value:0,min:0,max:2});

    //except the number area, click anywhere could close the collapse
    $(function(){
        $('#collapseNumber1 div').each(function(){
            $(this).attr('collapseNumber1','yes');
        });
        $('div').click(function(){
            if($("#collapseNumber1").hasClass("show")){
                if($(this).attr('collapseNumber1')==='yes')  {
                    return false;
                }
                else    {
                    $('#collapseNumber1').removeClass("show");
                }
            }
        });
    });

    $('.room-booking-number a').click(function(){
        if($('#Adults1').val() == 3){
            $('#Child1').val(0);
            $('#Child1').parent('.ui-spinner').find(".ui-spinner-down").attr("disabled",true).css("pointer-events","none");
            $('#Child1').parent('.ui-spinner').find(".ui-spinner-up").attr("disabled",true).css("pointer-events","none");
            //console.log($('#Child').parent('.spinner').find(".decrease"));
        }
        else{
            if($('#Child1').val() <2)
            {
                $('#Child1').parent('.ui-spinner').find(".ui-spinner-up").removeAttr("disabled").css("pointer-events","auto");
                $('#Child1').parent('.ui-spinner').find(".ui-spinner-down").removeAttr("disabled").css("pointer-events","auto");
            }

        }
    });

    //提交
$('#go_booking_room').click(function(){
    var http_host = $('input[name="http_host"]').val();
    //https://nonememberbe-qa.macausjm-glp.com/reservation/select-room?arrival_date=2020-12-24&departure_date=2020-12-25&rooms=1&adults=2&children=0&promotion_code=&hotel_code=GLPH&session_id=f58ac20c447a11eba5c3005056b3bb8e&group_by=room_type_cate&date=1608658125317
    var url = 'https://'+http_host+'/reservation/select-room?';
    url += 'arrival_date=' + changeDateType($('input[name="arrival_date_room"]').val());
    url += '&departure_date=' + changeDateType($('input[name="departure_date_room"]').val());
    url += '&rooms=' + $('#Rooms1').val();
    url += '&adults=' + $('#Adults1').val();
    url += '&children=' + $('#Child1').val();

    url += '&hotel_code='+$('input[name="hotelname_room"]').attr('data-value');
    url += '&group_by=room_type_cate';
    url += '&roomgroup=' + $('input[name="roomgroup"]').val();
    url += '&lang=' + changeLanguage($("#current_language").val());
    url += '&skin=tkl';
    window.open(url, '_blank');
    // window.location.href = url;
});

// var hotelnameChangeCode = function(hotelname){
//     if (hotelname.indexOf("Palazzo") >= 0) {
//         return 'PVMH';
//     }else if (hotelname.indexOf("LAGERFELD") >= 0) {
//         return 'TKLH';
//     }else{
//         return 'GLPH';
//     }
// }

var changeLanguage = function(language_code){
    if (language_code == 'zh-hant') {
        return 'tc';
    }else if(language_code == 'zh-hans'){
        return 'cn';
    }else{
        return 'en';
    }
}

var changeDateType = function(date){
    if ($("#current_language").val() != 'en') {
    date = date.match(/\d{4}.\d{1,2}.\d{1,2}/mg).toString();  
    date = date.replace(/[^0-9]/mg, '-');
    var srt = date.split('-'); 
    if (srt[1] < 10) {
        srt[1] = '0'+srt[1];
    }
    if (srt[2] < 10) {
        srt[2] = '0'+srt[2];
    }
    date = srt[0]+'-'+srt[1]+'-'+srt[2];

    }
    return moment(date).format(formatDateStr);

}

    //日历
    var formatDateStr='YYYY-MM-DD';
    var disabledDate = [];

    if($("#current_language").val() == 'zh-hant'){
        var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        var week_list=['日','一','二','三','四','五','六'];
    }else if($("#current_language").val() == 'zh-hans'){
        var week_list=['日','一','二','三','四','五','六'];
        var month_list=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
    }else{
        var week_list=['Sun','Mon','Tue','Wed','Thu','Fri','Sat'];
        var month_list=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
    }

    // $('#datepicker_from_room').val(fn_formatDate(moment().hours(0).minutes(0).seconds(0).format(formatDateStr)));
    // $('#datepicker_to_room').val(fn_formatDate(moment().hours(24).minutes(0).seconds(0).format(formatDateStr)));
    // $('#datepicker_from_room').val(fn_formatDate(moment('2021-07-15').format(formatDateStr)));
    // $('#datepicker_to_room').val(fn_formatDate(moment('2021-07-16').format(formatDateStr)));
    //触发to

    $('#datepicker_to_room').click(function(){
        $('#datepicker_from_room').click();
    });

    var minDate = new Date();

    var api_host = $('input[name="api_host"]').val();
    // api_host = 'https://wechat.glp.mo:9443/edip/api';
    var api_getUnCheckDate_address = api_host+'/Availability/GetUnCheckDate';
    var api_getFirstAvailDate_address = api_host+'/Availability/GetFirstAvailDate';
    // var hotelCode = 'GLPH';
    var hotelCode = $('input[name="hotelname_room"]').attr('data-value');
    var DateTo = moment().add(1, 'year').format(formatDateStr);
    var Channel = 'PAP';
    var RoomCode = $('input[name="roomtype"]').val();
    if(hotelCode != "" && hotelCode !=undefined) {
        $.ajax({
            //请求方式
            type: 'get',
            //发送请求的地址
            url: api_getFirstAvailDate_address,
            //服务器返回的数据类型
            dataType: 'json',
            data: {HotelCode: hotelCode, DateTo: DateTo, Channel: Channel, RoomCode: RoomCode, RateCodeGroup: '3'},
            success: function (data) {
                //请求成功函数内容
                //data = ajax_data;
                if (data.success === true) {
                    if (data['data']['firstAvailDate'] != null) {
                        var DateFrom = data['data']['firstAvailDate'];
                        minDate = new Date(DateFrom);
                        //$('#datepicker_from_room').data('daterangepicker').setStartDate(minDate);
                        $('#datepicker_from_room').val(fn_formatDate(moment(DateFrom).format(formatDateStr)));
                        $('#datepicker_to_room').val(fn_formatDate(moment(DateFrom).hours(24).minutes(0).seconds(0).format(formatDateStr)));
                    }
                    $.ajax({
                        //请求方式
                        type: 'get',
                        //发送请求的地址
                        url: api_getUnCheckDate_address,
                        //服务器返回的数据类型
                        dataType: 'json',
                        data: {
                            HotelCode: hotelCode,
                            DateTo: DateTo,
                            Channel: Channel,
                            RoomCode: RoomCode,
                            RateCodeGroup: '3'
                        },
                        success: function (data) {
                            //请求成功函数内容
                            //data = ajax_data;
                            if (data.success === true) {
                                disabledDate = data['data'];
                            }
                        },
                        error: function (jqXHR) {
                            //请求失败函数内容
                        }
                    });
                }
            },
            error: function (jqXHR) {
                //请求失败函数内容
            }
        });
    }

    $('#datepicker_from_room').daterangepicker({
        minDate:moment().hours(0).minutes(0).seconds(0), //设置开始日期
        //minDate:minDate,
        maxDate:moment().add(1, 'year'),
        format : formatDateStr, //控件中from和to 显示的日期格式
        autoApply: true,
        SpecialClassname:'OfferData',
        opens: 'left',
        autoUpdateInput:false,
        locale: {
            daysOfWeek: week_list,
            monthNames: month_list,
        },
        isInvalidDate: function(date) {
            if ($.inArray(date.format('YYYY-MM-DD'), disabledDate) != -1) {
                return true; 
            } else {
                return false; 
            }
            
        },
        isCustomDate: function(date) {
            return 'room_td';
        },
    }, function(start, end, label) {
        //格式化日期显示框
        $('#datepicker_from_room').val(fn_formatDate(start.format(formatDateStr)));
        $('#datepicker_to_room').val(fn_formatDate(end.format(formatDateStr)));
    });

    

});

var fn_formatDate=function(str){
    
    var now = new Date(str);
    var year = now.getFullYear(); 
    var month = now.getMonth();
    var day = now.getDate(); 
    if($("#current_language").val() == 'zh-hant'){
        var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        return year+'年' + enMonth[month] + day+'日';
    }else if($("#current_language").val() == 'zh-hans'){
        var enMonth=new Array('1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月');
        return year+'年' + enMonth[month] + day+'日';
    }else{
        var enMonth=new Array('January','February','March','April','May','June','July','August','September','October','November','December');
        return day +' '+ enMonth[month] +' '+ year;
    }
    
    };

    $('#datepicker_from_room').click(function(){
        $('.RoomDatastep').show();   
    });
    $(window).scroll(function(){
        $('.RoomDatastep').hide(); 
    });






